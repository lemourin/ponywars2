#ifndef ITEM_HPP
#define ITEM_HPP
#include "BaseObject.hpp"
#include <QMatrix4x4>

class QKeyEvent;
class QTouchEvent;
class QMouseEvent;

namespace SceneGraph {

class Window;
class Node;
class TransformNode;

class Item: protected BaseObject {
    private:
        friend class Window;
        friend class Renderer;

        Window* m_window;
        TransformNode* m_itemNode;
        Node* m_node;
        unsigned m_state;
        QMatrix4x4 m_matrix;

        enum State {
            ScheduledUpdate = 1 << 0,
            ParentChanged = 1 << 1,
            ModelMatrixChanged = 1 << 2,
            HasFocus = 1 << 3
        };

        void setWindow(Window*);

    protected:
        virtual Node* synchronize(Node* old);

        virtual void keyPressEvent(QKeyEvent*);
        virtual void touchEvent(QTouchEvent*);
        virtual void mouseMoveEvent(QMouseEvent*);

    public:
        Item(Item* parent = nullptr);
        ~Item();

        Item* firstChild() const;
        Item* next() const;
        Item* parent() const;

        void appendChild(Item*);
        void removeChild(Item*);

        inline Window* window() const { return m_window; }

        inline const QMatrix4x4& matrix() const { return m_matrix; }
        void setMatrix(const QMatrix4x4& m);

        inline bool focus() const { return m_state & HasFocus; }
        void setFocus(bool);

        void update();
};

}

#endif // ITEM_HPP
