#ifndef WINDOW_HPP
#define WINDOW_HPP
#include <QQuickView>
#include <QQuickItem>
#include "Item.hpp"

class QOpenGLTexture;

namespace SceneGraph {

class Renderer;

class Window: public QQuickView {
    private:
        friend class Item;
        friend class Renderer;

        Renderer* m_renderer;
        Item m_root;
        Item* m_focusItem;
        std::vector<Item*> m_updateItem;
        std::vector<Item*> m_nextFrame;
        std::vector<Node*> m_destroyedItemNode;
        std::vector<Node*> m_destroyedNode;

        void onSceneGraphInitialized();
        void onSceneGraphInvalidated();
        void onBeforeRendering();
        void onBeforeSynchronizing();

        void onItemDestroyed(Item*);
        void scheduleUpdate(Item*);

    protected:
        void keyPressEvent(QKeyEvent *);
        void touchEvent(QTouchEvent *);
        void mousePressEvent(QMouseEvent *);
        void mouseMoveEvent(QMouseEvent *);

    public:
        Window(QWindow* window = nullptr);
        ~Window();

        inline Item* rootItem() { return &m_root; }
        inline Item* focusItem() const { return m_focusItem; }

        inline void scheduleSynchronize() { contentItem()->update(); }

        QOpenGLTexture* texture(const char* path);
};

}

#endif // WINDOW_HPP
