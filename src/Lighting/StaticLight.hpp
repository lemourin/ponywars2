#ifndef STATICLIGHT_HPP
#define STATICLIGHT_HPP
#include "Light.hpp"
#include "QBox2D/QBody.hpp"
#include "QBox2D/Fixture/Box2DBox.hpp"

class DynamicLight;

class StaticLight: public Light {
    private:
        friend class DynamicLight;

        bool m_dynamicShadows;
        Box2DBox m_fixture;
        DynamicLight* m_dynamicLight;

    protected:
        void synchronize();
        void geometryChanged(const QRectF &newGeometry,
                             const QRectF &oldGeometry);
        void itemChange(ItemChange, const ItemChangeData &);

    public:
        StaticLight(QQuickItem* = nullptr);
        ~StaticLight();

        void initialize();

        inline bool dynamicShadows() const { return m_dynamicShadows; }
        inline void setDynamicShadows(bool d) { m_dynamicShadows = d; }

        inline DynamicLight* dynamicLight() const { return m_dynamicLight; }

        bool read(const QJsonObject &);
        bool write(QJsonObject &) const;

};

#endif // STATICLIGHT_HPP
