#ifndef CUBE_HPP
#define CUBE_HPP

#include <QQuickItem>
#include <QSGGeometry>
#include <QSGGeometryNode>
#include <QSGVertexColorMaterial>

class Cube: public QQuickItem {
    private:
        Q_OBJECT

    protected:
        QSGNode* updatePaintNode(QSGNode *, UpdatePaintNodeData *);
        void timerEvent(QTimerEvent *);

    public:
        Cube(QQuickItem* = nullptr);
};

class CubeGeometry: public QSGGeometry {
    private:
        struct VertexData {
            float x, y, z, r, g, b, a;
        };

        static std::vector<VertexData> vertices();
        static std::vector<uint> indices();
        static const QSGGeometry::AttributeSet& attribs();

    public:
        CubeGeometry();
};

class CubeNode: public QSGGeometryNode {
    private:
        CubeGeometry m_geometry;
        QSGVertexColorMaterial m_material;

    public:
        CubeNode();
};


#endif // CUBE_HPP
