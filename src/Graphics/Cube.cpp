#include "Cube.hpp"
#include <QQuickWindow>
#include "QBox2D/QWorld.hpp"
#include <QSGVertexColorMaterial>
#include <QSGTransformNode>
#include "Utility/Utility.hpp"

Cube::Cube(QQuickItem* parent):
    QQuickItem(parent) {
    setFlag(ItemHasContents);
    startTimer(16);
    forceActiveFocus();
}

void Cube::timerEvent(QTimerEvent*) {
    update();
}

QSGNode* Cube::updatePaintNode(QSGNode* old, UpdatePaintNodeData*) {
    QSGTransformNode* rootNode = static_cast<QSGTransformNode*>(old);
    if (!rootNode) {
        rootNode = new QSGTransformNode;

        CubeNode* cubeNode = new CubeNode;
        cubeNode->setFlag(QSGNode::OwnedByParent);
        rootNode->appendChildNode(cubeNode);
    }


    const QWorld* world = Utility::findAncestor<const QWorld>(this);
    QRectF rect = world->mapRectFromScene(QRectF(QPointF(), window()->size()));
    QPointF p = mapFromItem(world, rect.center());

    QMatrix4x4 matrix;
    matrix.perspective(60, 1, 0.1, 100);
    matrix.lookAt(QVector3D(p.x(), p.y(), 10),
                  QVector3D(p.x(), -p.y(), 0),
                  QVector3D(0, 1, 0));

    rootNode->setMatrix(matrix);

    return rootNode;
}

CubeGeometry::CubeGeometry(): QSGGeometry(attribs(), 24, 34) {
    std::vector<VertexData> pts = vertices();

    for (size_t i=0; i<pts.size(); i++)
        static_cast<VertexData*>(vertexData())[i] = pts[i];

    std::vector<uint> inds = indices();
    for (size_t i=0; i<inds.size(); i++)
        indexDataAsUShort()[i] = inds[i];
}

std::vector<CubeGeometry::VertexData> CubeGeometry::vertices() {
    return {
        { -1, -1,  1, 0.3, 0.3, 0, 1 },
        {  1, -1,  1, 0.3, 0.3, 0, 1 },
        { -1,  1,  1, 0.3, 0.3, 0, 1 },
        {  1,  1,  1, 0.3, 0.3, 0, 1 },

        {  1, -1,  1, 1, 0, 1, 1 },
        {  1, -1, -1, 1, 0, 1, 1 },
        {  1,  1,  1, 1, 0, 1, 1 },
        {  1,  1, -1, 1, 0, 1, 1 },

        {  1, -1, -1, 1, 0, 0, 1 },
        { -1, -1, -1, 1, 0, 0, 1 },
        {  1,  1, -1, 1, 0, 0, 1 },
        { -1,  1, -1, 1, 0, 0, 1 },

        { -1, -1, -1, 0, 1, 0, 1 },
        { -1, -1,  1, 0, 1, 0, 1 },
        { -1,  1, -1, 0, 1, 0, 1 },
        { -1,  1,  1, 0, 1, 0, 1 },

        { -1, -1, -1, 0, 0, 1, 1 },
        {  1, -1, -1, 0, 0, 1, 1 },
        { -1, -1,  1, 0, 0, 1, 1 },
        {  1, -1,  1, 0, 0, 1, 1 },

        { -1,  1,  1, 0, 1, 1, 1 },
        {  1,  1,  1, 0, 1, 1, 1 },
        { -1,  1, -1, 0, 1, 1, 1 },
        {  1,  1, -1, 0, 1, 1, 1 }
    };
}

std::vector<uint> CubeGeometry::indices() {
    return {
        0, 1, 2, 3, 3,
        4, 4, 5, 6, 7, 7,
        8, 8, 9, 10, 11, 11,
        12, 12, 13, 14, 15, 15,
        16, 16, 17, 18, 19, 19,
        20, 20, 21, 22, 23
    };
}

const QSGGeometry::AttributeSet& CubeGeometry::attribs() {
    static QSGGeometry::Attribute att[] = {
        QSGGeometry::Attribute::create(0, 3, GL_FLOAT, true),
        QSGGeometry::Attribute::create(1, 4, GL_FLOAT, false)
    };

    static QSGGeometry::AttributeSet set = {
        1,
        sizeof(VertexData),
        att
    };

    return set;
}


CubeNode::CubeNode() {
    setGeometry(&m_geometry);
    setMaterial(&m_material);

    m_material.setFlag(QSGMaterial::Blending, false);
}
