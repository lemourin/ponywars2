#ifndef ACTION_HPP
#define ACTION_HPP
#include <QQuickItem>

class World;

class Action: public QQuickItem {
    private:
        Q_OBJECT

        World* m_world;

    protected:
        inline virtual void onInit() { }
        virtual void focusChanged();

    public:
        Action(QQuickItem* = nullptr);

        Q_INVOKABLE virtual void reset();

        inline World* world() const { return m_world; }
        inline void setWorld(World* w) { m_world = w; }

        Q_INVOKABLE inline virtual void finished() { }

};

#endif // ACTION_HPP
