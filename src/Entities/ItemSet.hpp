#ifndef ITEMSET_HPP
#define ITEMSET_HPP
#include <QQuickItem>

class ItemSet: public QQuickItem {
    private:
        Q_OBJECT

    public:
        ItemSet(QQuickItem* = nullptr);

        Q_INVOKABLE void dump(QString fileName);
        Q_INVOKABLE void load(QString fileName);

        void write(QJsonObject&) const;
        void read(const QJsonObject&);
};

#endif // ITEMSET_HPP
