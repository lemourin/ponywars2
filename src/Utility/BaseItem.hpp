#ifndef BASEITEM_HPP
#define BASEITEM_HPP
#include <QQuickItem>

class DisplayItem;

class BaseItem: public QQuickItem {
    private:
        friend class DisplayItem;

        DisplayItem* m_displayItem;

    protected:
        void geometryChanged(const QRectF &newGeometry,
                             const QRectF &oldGeometry);

    public:
        explicit BaseItem(QQuickItem* parent = nullptr);
        ~BaseItem();

        inline DisplayItem* displayItem() const { return m_displayItem; }
};

#endif // BASEITEM_HPP
